using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyItemOnClick : MonoBehaviour
{
    private bool IsAtArea = false;
    public GameObject portal;

    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.E) && IsAtArea == true)
        {
            Destroy(portal);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            IsAtArea = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        IsAtArea = false;
    }
}
