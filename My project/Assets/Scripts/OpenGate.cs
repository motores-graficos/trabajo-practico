using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenGate : MonoBehaviour
{
    private Animator anim;
    private bool IsPowered = false;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (IsPowered == true)
        {
            anim.SetTrigger("OpenGate");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PowerCapsule")
        {
            IsPowered = true;
        }
        //if (other.CompareTag("PowerCapsule"))
        //{
        //    GetComponent<Animator>().SetTrigger("OpenGate");
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        IsPowered = false;
    }
}
