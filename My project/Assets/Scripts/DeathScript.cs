using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScript : MonoBehaviour
{
    public float timeInAir = 0f;
    public float deathTimer = 10f;

    // Update is called once per frame
    void Update()
    {
        Death();
    }
    void Death()
    {
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            timeInAir = 0f;
        }
        if (!controller.isGrounded)
        {
            timeInAir += Time.deltaTime;

        }
        if (timeInAir >= deathTimer)
        {
            Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
            print("Shiiii you dead");
        }
    }
}
