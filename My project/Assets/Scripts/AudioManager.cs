using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.AudioSrc = gameObject.AddComponent<AudioSource>();
            s.AudioSrc.clip = s.SoundClip;
            s.AudioSrc.volume = s.Volume;
            s.AudioSrc.pitch = s.pitch;
            s.AudioSrc.loop = s.loop;
        }

    }
    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found");
            return;
        }
        else
        {
            s.AudioSrc.Play();
        }
    }

    public void PauseSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found");
            return;
        }
        else
        {
            s.AudioSrc.Pause();
        }
    }

}

