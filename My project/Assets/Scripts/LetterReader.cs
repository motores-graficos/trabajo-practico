using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterReader : MonoBehaviour
{
    public GameObject visualLetter;
    public bool activate;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && activate == true)
            visualLetter.SetActive(true);

        if (Input.GetKeyDown(KeyCode.Escape) && activate == true)
            visualLetter.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            activate = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            activate = false;
            visualLetter.SetActive(false);
        }
    }
}
